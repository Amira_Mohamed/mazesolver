package eq.edu.alexu.csd.ds.maze.csd17;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Scanner;

	public class App {

	
		public static void main(String[] args) throws IOException {
			Scanner input = new Scanner (System.in);
			int width = 0,height = 0,r=0,c=0;char[][]grid = null;
			ReadFile reader;
			
			System.out.println("Choose: Solve MAZE Random/SourceFIle");
			int f = input.nextInt();
			if (f==1){
				CreateMazeRandomEasy random = new CreateMazeRandomEasy();
			 	random.getGrid();
				reader = new ReadFile("Map.txt");
			}
			
			else {
				reader = new ReadFile("grid.txt");
			}
				
			//reader = new ReadFile("grid.txt");
			grid = reader.getGrid();
			width = reader.getWidth();
			height = reader.getHeight();
			r = reader.getStart().x;
			c = reader.getStart().y;
			
			try {
				System.out.println("Enter The Alogrithm Solver Name: BFS/DFS");
				Class t = Class.forName("eq.edu.alexu.csd.ds.maze.csd17."+input.next());
						
				AlgorithmSolver solver = (AlgorithmSolver) t.newInstance();
				
				if (solver.search (grid,height,width,r,c)){
					ReadFile read = new ReadFile("solution.txt");
					char[][] output = read.getGrid();
					int h = read.height;
					int w = read.width;
					for (int i=0;i<h;i++){
						for (int j=0;j<w;j++){
							System.out.print (output[i][j]);
						}
						System.out.println ();
					}
				}

				else {System.out.println("Can not be solved");return;}

			} catch (Exception e) {	e.printStackTrace();
				System.out.println("NOT Valid Algorism Name");return; 
			}
		
		
		
	}
	}


