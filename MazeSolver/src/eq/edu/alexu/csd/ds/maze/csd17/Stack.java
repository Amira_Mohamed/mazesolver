package eq.edu.alexu.csd.ds.maze.csd17;

import java.util.EmptyStackException;

	public class Stack implements MyStack{
		private MyLinkedList l = new Slist();
		
		@Override
		// insert an element in specified position
		public void add(int index, Object element) { // linked list is responsible for exception
			l.add(index, element);
		}
		@Override
		// Removes the element at the top of stack and returns that element.
		// from head
		public Object pop() {
			if (l.size()==0) throw new EmptyStackException();
			Object p = l.get(0);
			l.remove(0);
			return p;
		}
		@Override
		public Object peek(){
			if (l.size()==0) throw new EmptyStackException();
			return l.get(0);
		}

		@Override
		public void push(Object element) {
			l.add(0, element);	
		}
		@Override
		public boolean isEmpty() {
			if (l.isEmpty()==true) return true;
			return false;
		}

		@Override
		public int size() {
			return l.size();
		}
	}
