package eq.edu.alexu.csd.ds.maze.csd17;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;

	public class CreateMazeRandomHard {
		private  int x;
		private  int y;
		private  int[][] maze;
	 
		public  CreateMazeRandomHard() {			
		       x=(int)(4+Math.random()*20);
		       y=(int)(4+Math.random()*20);
		       maze = new int[x][y];
			generateMaze(0, 0);

		}
			private void generateMaze(int cx, int cy) {
				DIR[] dirs = DIR.values();
				Collections.shuffle(Arrays.asList(dirs));
				for (DIR dir : dirs) {
					int nx = cx + dir.dx;
					int ny = cy + dir.dy;
					if (between(nx, x) && between(ny, y)
							&& (maze[nx][ny] == 0)) {
						maze[cx][cy] |= dir.bit;
						maze[nx][ny] |= dir.opposite.bit;
						generateMaze(nx, ny);
					}
				}
			}
		 
			private static boolean between(int v, int upper) {
				return (v >= 0) && (v < upper);
			}
		 
			private enum DIR {
				N(1, 0, -1), S(2, 0, 1), E(4, 1, 0), W(8, -1, 0);
				private final int bit;
				private final int dx;
				private final int dy;
				private DIR opposite;
		 
				// use the static initializer to resolve forward references
				static {
					N.opposite = S;
					S.opposite = N;
					E.opposite = W;
					W.opposite = E;
				}
		 
				private DIR(int bit, int dx, int dy) {
					this.bit = bit;
					this.dx = dx;
					this.dy = dy;
				}
			};
			
			
		public void display() throws IOException{
			 String s;
			 File file = new File("Map.txt");
			 PrintWriter writer = new PrintWriter(file);
			 writer.print("");
			 writer.close();
			 FileWriter fw = new FileWriter(file.getAbsoluteFile());
			 BufferedWriter output = new BufferedWriter(fw);
			 String par1 = Integer.toString(x);
			 String par2 = Integer.toString(y);
			 output.write(par1+" "+par2);
			 output.newLine();
			for (int i = 0; i < y; i++) {
				// draw the north edge
				for (int j = 0; j < x; j++) {
					
	                if(j==0 && i==0){
	                	if ((maze[j][i] & 1) == 0) s="#S";
	                	else s="#.";}
	                else {
                	if ((maze[j][i] & 1) == 0) s="##";
                	else s="#.";}
	                output.write(s);
				}                                                         
				 output.write("#");
				 output.newLine();
				// draw the west edge
	                        
				for (int j = 0; j < x; j++){           
					//System.out.print((maze[j][i] & 8) == 0 ? "#." : "..");
					if ((maze[j][i] & 8) == 0 ) s= "#.";
					else s="..";
					output.write(s);
				} 
	            if(i==y-1) {output.write("E");output.newLine();}
	                   // System.out.println("E");
	            else
				   //System.out.println("#");
	            {output.write("E");output.newLine();}
			}
			// draw the bottom line
			for (int j = 0; j < x; j++) {
				//System.out.print("##");
				output.write("##");
			}
			output.write("#");output.newLine();
			output.close();
		}

	}
