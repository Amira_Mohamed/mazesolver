package eq.edu.alexu.csd.ds.maze.csd17;

	public interface AlgorithmSolver {
		boolean search(char[][] grid,int height,int width,int r,int c);
	}
