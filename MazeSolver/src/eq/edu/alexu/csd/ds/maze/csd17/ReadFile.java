package eq.edu.alexu.csd.ds.maze.csd17;

import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ReadFile {
	String path;
	int width,height,r,c;
	
	public ReadFile (String s){path = s;}
	
	char [][] getGrid () throws FileNotFoundException {
		char [][] grid=null;
		try {
			Scanner skan = new Scanner (new File (path));
			height = skan.nextInt();
			width = skan.nextInt();
			grid = new char [height][width];
			for (int i=0;i<height;i++){
				String s = skan.next();
				for (int j=0;j<width;j++){
					grid[i][j] = s.charAt(j);
					if (grid[i][j]=='S') {r=i;c=j;}
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return grid;
	}
	int getWidth (){
		return width;
	}
	int getHeight (){
		return height;
	}
	Point getStart (){
		return new Point(r,c);
	}
}
