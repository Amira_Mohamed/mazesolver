package eq.edu.alexu.csd.ds.maze.csd17;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

 	public class CreateMazeRandomEasy {
		int height=4,width=4;
		void getGrid () throws IOException{
		width =(int)(4+Math.random()*20); // create random width
		height = (int)(4+Math.random()*20); // create random height
		char[][] grid = new char[height][width];
		for (int i=0;i<height;i++){
			for (int j=0;j<width;j++){
				grid[i][j] = '#';
			}
		}
		int r=0,c=(int)(Math.random()*(width));

		
		grid[r][c] = 'S'; 
		int rand;
		boolean end=false;
		int min = (width+height)/2;
		int run = min+width%height;
		int i=0;
		int counter=0;
		while (!end&& i<run ){
			rand = (int)(Math.random()*4);
			switch (rand){
			case 0: {
				if (r+1<height){
					if (grid[r+1][c]=='#'){grid[++r][c]='.';counter++;}
					
				}
				else {
					if (counter >= min) {grid[r][c]='E';end=true;}
					else {} // do nothing
				}
				break;
			}
			case 1: {
				if (r-1>=0){
					if (grid[r-1][c]=='#'){grid[--r][c]='.';counter++;}
					
				}
				else {
					if (counter >= min) {grid[r][c]='E';end=true;}
					else {} // do nothing
				}
				break;
			}
			case 2: {
				if (c+1<width){
					if (grid[r][c+1]=='#'){grid[r][++c]='.';counter++;}
					
				}
				else {
					if (counter >= min) {grid[r][c]='E';end=true;}
					else {} // do nothing
				}
				break;
			}
			case 3: {
				if (c-1>=0){
					if (grid[r][c-1]=='#'){grid[r][--c]='.';counter++;}
					
				}
				else {
					if (counter >= min) {grid[r][c]='E';end=true;}
					else {} // do nothing
				}
				break;
			}
			
		}i++;
			
	} 
		if (!end){
			r++;
			while(r < (height-1)){
				grid[r][c]='.';
				r++;
			}
			grid[r][c]='E';
		}
		
		 File file = new File("Map.txt");
		 PrintWriter writer = new PrintWriter(file);
		 writer.print("");
		 writer.close();
		 FileWriter fw = new FileWriter(file.getAbsoluteFile());
		 BufferedWriter output = new BufferedWriter(fw);
		 String par1 = Integer.toString(height);
		 String par2 = Integer.toString(width);
		 output.write(par1+" "+par2);
		 output.newLine();
		
		for (int a=0;a<height;a++){
			for (int j=0;j<width;j++){
				if (grid[a][j]=='#'){
					int g = (int)(Math.random()*2);
					if (g==0) grid[a][j] = '.';
				}
				output.write(grid[a][j]);
			}
			output.newLine();
		}
		output.close();
 	}
	
 }
