package eq.edu.alexu.csd.ds.maze.csd17;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

	public class DFS implements AlgorithmSolver{

		public boolean valid (char[][] grid,int height,int width,int r,int c){
			if (c>=width||r>=height||r<0||c<0||grid[r][c] == '#') return false;
				return true;
		}
		public void printPath (Pos p,char[][] grid,int height,int width){
			try {
			 File file = new File("solution.txt");
			 PrintWriter writer = new PrintWriter(file);
			 writer.print("");
			 writer.close();
			 FileWriter fw = new FileWriter(file.getAbsoluteFile());
			 BufferedWriter output = new BufferedWriter(fw);
			 p = p.parent;
	         while (grid [p.ro][p.col]!='S'){
	        	 grid[p.ro][p.col]='*';
	        	 p = p.parent;
	         }
			 String par1 = Integer.toString(height);
			 String par2 = Integer.toString(width);
			 output.write(par1+" "+par2);
			 output.newLine();
	         for (int i=0;i<height;i++){
	        	 for (int j=0;j<width;j++){
	        		  String s = Character.toString(grid[i][j]);
	        		 output.write(s);
	        	 }output.newLine();
	         } output.close();
			}
			catch ( IOException e ) {
	            e.printStackTrace();}
		}
		
		public boolean search (char[][] grid,int height,int width,int r,int c){
			boolean visited[][]=new boolean[height][width];
			Pos p=new Pos (null,r,c,null);
			Stack s = new Stack ();
			s.push(p); // add start position
			while (!s.isEmpty()){
				p = (Pos) s.pop();
				if (!visited[p.ro][p.col]){
					visited[p.ro][p.col] = true;
					if (grid[p.ro][p.col]=='E') {
						printPath(p,grid,height,width);
						return true;
					}
					else { // search in all directions
						if (valid(grid,height,width,p.ro+1,p.col)&&!visited[p.ro+1][p.col])  s.push
						(new Pos(p,p.ro+1,p.col,"down"));
						if (valid(grid,height,width,p.ro-1,p.col)&&!visited[p.ro-1][p.col])  s.push
						(new Pos(p,p.ro-1,p.col,"up"));
						if (valid(grid,height,width,p.ro,p.col+1)&&!visited[p.ro][p.col+1])  s.push
						(new Pos(p,p.ro,p.col+1,"right"));
						if (valid(grid,height,width,p.ro,p.col-1)&&!visited[p.ro][p.col-1])  s.push
						(new Pos(p,p.ro,p.col-1,"left"));
					}
				}
			}
			return false;
		}
	}

